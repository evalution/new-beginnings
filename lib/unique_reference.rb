require 'securerandom'

module UniqueReference
  def self.populate
    SecureRandom.alphanumeric(6).upcase
  end
end