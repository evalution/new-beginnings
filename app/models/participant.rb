require "unique_reference"

class Participant < ActiveRecord::Base
  DISPLAYED_ATTRIBUTES = [
    :reference,
    :name,
    :date_of_birth,
    :address,
    :phone_number
  ]

  validates :reference, uniqueness: true
  validates :name, presence: true
  validates :date_of_birth, presence: true
  validates :address, presence: true
  validates :phone_number, presence: true

  before_create :populate_reference

  private

  # TODO: Update to use the external API
  def populate_reference
    self.reference = UniqueReference.populate
  end
end
