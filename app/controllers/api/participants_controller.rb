module Api
  class ParticipantsController < ApplicationController
    respond_to :json
    before_action :verify_requested_format!

    before_action :participant, only: [:show, :update, :destroy]
    before_action :participant_params, only: [:create, :update]

    # GET /api/participants
    def index
      @participants = Participant.all
      respond_with to_json_response(@participants)
    end

    # GET /api/participant/:reference
    def show
      respond_with to_json_response(@participant)
    end

    # POST /api/participants
    def create
      @participant = Participant.create!(participant_params)
      respond_with to_json_response(@participant),
        status: :created,
        location: api_participant_url(@participant)
    rescue ActiveRecord::RecordInvalid => e
      respond_with(
        { message: e.message },
        status: :unprocessable_entity,
        location: api_participants_url,
      )
    end

    # PUT /api/participant/:reference
    def update
      @participant.update!(participant_params)
      head :no_content
    rescue ActiveRecord::RecordInvalid => e
      respond_with({ message: e.message }, status: :unprocessable_entity)
    end

    # DELETE /api/participants/:reference
    def destroy
      @participant.destroy
      head :no_content
    end

    private

    def participant
      permitted_params = params.permit("reference")
      @participant = Participant.find_by_reference!(permitted_params["reference"])
    rescue ActiveRecord::RecordNotFound => e
      respond_with({ message: e.message }, status: :not_found)
    end

    def participant_params
      params.require(:participant).permit(:name, :date_of_birth, :address, :phone_number)
    rescue ActionController::ParameterMissing => e
      respond_with({ message: e.message }, status: :bad_request)
    end

    def to_json_response(obj)
      obj.to_json(only: Participant::DISPLAYED_ATTRIBUTES)
    end
  end
end