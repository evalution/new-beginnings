require 'rails_helper'

RSpec.describe Participant, type: :model do
  context 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:date_of_birth) }
    it { is_expected.to validate_presence_of(:address) }
    it { is_expected.to validate_presence_of(:phone_number) }
  end

  context 'when a participant is created' do
    let(:date_of_birth) { Date.parse('2000-12-13') }
    let(:participant) { Participant.new(
      name: 'Jane',
      date_of_birth: '2000-12-13',
      address: 'address',
      phone_number: '000',
    )}

    before do
      participant.save
    end

    it 'saves the participant' do
      expect(participant).to be_persisted
    end

    it 'populates a reference' do
      expect(participant.reference).to be_kind_of(String)
    end

    it 'saves all the fields correctly' do
      expect(participant.name).to eq 'Jane'
      expect(participant.date_of_birth).to eq date_of_birth
      expect(participant.address).to eq 'address'
      expect(participant.phone_number).to eq '000'
    end
  end
end
