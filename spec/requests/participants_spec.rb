require 'rails_helper'

RSpec.describe 'Participants Requests', type: :request do
  let!(:participants) { create_list(:participant, 10) }
  let(:json) { JSON.parse(response.body) }

  describe 'GET /api/participants' do
    before { get '/api/participants' }

    it 'returns 200' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns participants' do
      expect(json.size).to eq(participants.size)
    end
  end

  describe 'GET /api/participant/:reference' do
    let(:participant) { participants.last }
    before { get "/api/participants/#{reference}" }

    context 'with an existing reference' do
      let(:reference) { participant.reference }
      it 'returns 200' do
        expect(response).to have_http_status(:ok)
      end

      it 'returns the participant\'s details' do
        expect(json).to eq({
          "reference" => participant.reference,
          "name" => participant.name,
          "date_of_birth" => participant.date_of_birth.to_s,
          "address" => participant.address,
          "phone_number" => participant.phone_number,
      })
      end
    end

    context 'when reference does not exists' do
      let(:reference) { "32" }

      it 'returns 404' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'POST /api/participants' do
    before { post '/api/participants', params: params }

    context 'with the wrong parameters' do
      let(:params) { { "worng": false } }

      it 'returns 400' do
        expect(response).to have_http_status(:bad_request)
      end
    end

    context 'with missing parameters' do
      let(:params) { { participant: {
        date_of_birth: Date.today - 20.years,
        address: "my address",
        phone_number: "00000000000",
      } } }

      it 'returns 422' do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'responds with the new participant 201' do
        expect(json).to include(
          "message" => "Validation failed: Name can't be blank",
        )
      end
    end

    context 'with the correct parameters' do
      let(:params) { { participant: {
        name: "Jo Doe",
        date_of_birth: "12-12-2000",
        address: "my address",
        phone_number: "00000000000",
      } } }

      it 'returns 201' do
        expect(response).to have_http_status(:created)
      end

      it 'responds with the new participant' do
        expect(json).to include(
          "reference" => be_kind_of(String),
          "address" => "my address",
          "date_of_birth" => "2000-12-12",
          "name" => "Jo Doe",
          "phone_number" => "00000000000",
        )
      end
    end
  end

  describe 'PUT /api/participants/:reference' do
    let(:params) { {
      participant: {
        name: "Janet Doe"
      } } }
      let(:participant) { participants.last }
      let(:reference) { participant.reference }

    before { put "/api/participants/#{reference}", params: params }

    context 'with wrong parameters' do
      let(:params) { {
      participants: {
        name: "Janet Doe"
      } } }

      it 'returns 400' do
        expect(response).to have_http_status(:bad_request)
      end
    end

    context 'with a missing reference' do
      let(:reference) { "32" }

      it 'returns 404' do
        expect(response).to have_http_status(:not_found)
      end
    end

    context 'with the correct parameters' do
      it 'returns 204' do
        expect(response).to have_http_status(:no_content)
      end

      it 'updates the name' do
        participant.reload
        expect(participant.name).to eq "Janet Doe"
      end
    end
  end

  describe 'DELETE /api/participants/:reference' do
    before { delete "/api/participants/#{reference}" }

    context 'with a missing reference' do
      let(:reference) { "32" }

      it 'returns status code 404' do
        expect(response).to have_http_status(:not_found)
      end
    end

    context 'with an existing reference' do
      let(:participant) { participants.last }
      let(:reference) { participant.reference }

      it 'returns status code 204' do
        expect(response).to have_http_status(:no_content)
      end
    end
  end
end
