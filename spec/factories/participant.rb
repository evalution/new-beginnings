FactoryBot.define do
  factory :participant do
    sequence(:name) { |n| "John Doe #{n}" }
    date_of_birth  { Date.today - 30.years }
    phone_number { "07777777777" }
    address { "Address 1" }
  end
end