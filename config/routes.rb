Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api, defaults: {format: 'json'} do
    resources :participants, only: [:index, :create]
    resources :participants, only: [:show, :update, :destroy], param: :reference
  end
end
