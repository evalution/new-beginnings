FROM ruby:3.0.0-slim-buster

RUN apt-get update -qq && apt-get install -y build-essential libsqlite3-dev

RUN mkdir /app
WORKDIR /app

ADD Gemfile* /app
RUN bundle install --without development test

ADD . /app

EXPOSE 4567

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0", "-p", "4567"]