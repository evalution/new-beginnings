# New Beginnings - Clinical Trial Participants Registry

This is an API written with Ruby on Rails. This application uses SQLite, which is a library used to implement a lightweight  SQL database engine.

## System dependencies/Configuration

Requirements:
* Docker

To make the application easier to run on different machines without much configuration, I decided to use Docker both for running the API itself and the tests. I have added 2 different `Dockerfile`s for that reason: one to run the server with the production dependencies and one to run the tests.

## How to run the API

To run the server with the API:

```sh
docker build -t new-beginnings-demo .
docker run -p 80:4567 new-beginnings-demo
```

### Example requests (Happy Path)

List all participants (it will be empty when the app starts):
```sh
curl localhost/api/participants
```

Create a participant:
```sh
curl -X POST localhost/api/participants -H "Content-Type: application/json" -d '{"name": "Eva", "address": "new address", "phone_number": "07777777777", "date_of_birth": "1900-12-12"}'
```

Update a participant (you will need to know their reference):
```sh
curl -X PUT localhost/api/participants/12ABC3 -H "Content-Type: application/json" -d '{ "participant": { "name": "Evangelia" } }'
```

Delete a participant (you will need to know their reference):
```sh
curl -X DELETE localhost/api/participants/12ABC3
```

## How to run the test suite

I have used RSpec for the tests. To run the tests:

```sh
docker build -t new-beginnings-tests  -f Dockerfile.test .
docker run new-beginnings-tests
```

## Important files

The Rails framework can be noisy (it has created automatically many files that we do not need for this project at this time). In case you are not familiar with it, I am providing a list of the files I have updated:

* [Participant Model](./app/models/participant.rb)
* [Participants Controller](./app/controllers/api/participants_controller.rb)
* [Participant Model Tests](./spec/models/participant_spec.rb)
* [Participant Request Tests](./spec/requests/participants_spec.rb)

## Design Dessigns and Trade-Off

* To begine wiht, I started creating a very light Rack application, instead of using Rails. I decided to use Rails after a bit and started over when I needed a lot of Gems that were included with Rails to do validations etc. The application I built is not as lightweight as it would be with Rack or a framework like Sinatra, but Rails helped me move faster.
* I tried to create the simplest model possible based on the instructions I got. If I had more time, I would have added more sophisticated validations on the fields (e.g. validate string phone numbers). I would also split the name to first name/last name, this is more vercetile in case we decide in the future to use the app to send emails and other communication.
* I have created a module to generate the unique reference, as a placeholder since I do not have access to the assumed separate API. It uses SecureRandom for generating the references (the 6 digits I have provided should generate 1.5 billion combinations which should be enough to represent the 100,000 participants). I used alphanumeric to make it more easy to spell on the phone and avoid "identity theft" (e.g. just integers would be too easy to guess).
* Since this is an API used by some other team or external users, I would also add detailed documentation about each method in a real world scenario.
* The spec was not asking for authentication, but if it did I would implement it with Oauth2 refresh and access tokens.
* I should also have marked the API with a version in the routes e.g. V1, in case I decide to change the API in the future when I need to create a newer version.
* I decided to use request tests instead of controller tests because they are testing both the route and the controller code implicitely. If I added the controller tests there would be duplication.