class CreateParticipants < ActiveRecord::Migration[6.1]
  def change
    create_table :participants do |t|
      t.string :reference
      t.string :name
      t.date :date_of_birth
      t.string :address
      t.string :phone_number

      t.timestamps
    end
  end
end
